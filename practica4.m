% Practica 4
% Elabora:
%   Dante Fernando Bazaldua Huerta
%   Leonardo Alberto L?pez Romero

clear all, clc;

%% Parte 2
figure
set(gcf, 'Name', 'Comparaci?n de m?todos de filtrado espaciales', 'NumberTitle', 'Off');
% Lee la imagen y crea un filtro lineal
img1 = imread('testpattern1.png');
Lfilter = fspecial('motion', 15, 45);
%convolucion
conv = @(l)sum(sum(double(l).*Lfilter));
% medir tiempo de imfilter
img1Time = cputime;
%imfilter
img1_imf = imfilter(img1,Lfilter);
img1TimeEnd = cputime - img1Time;
fprintf('Tiempo de imfilter %f\n',img1TimeEnd)
subplot(1,2,1),imshow(img1_imf, []), title('Funci?n ImFilter');
%nlfilter
img1_g = rgb2gray(img1);
img1_nlf = cputime;
tp1final = nlfilter(img1_g, [11 11], conv);
img1TimeEndnl = cputime - img1_nlf;
fprintf('Tiempo de nlfilter %f\n',img1TimeEndnl)
subplot(1,2,2),imshow(tp1final, []),title('Funci?n NlFilter');

%% Parte 3
clear; clc;
% Creaci?n de filtros
% promedio
matrizProm = [1 1 1 ; 1 1 1 ; 1 1 1];
filtroProm = matrizProm./9;
% gaussiano
matrizGauss = [1 2 1 ; 2 4 2 ; 1 2 1];
filtroGauss = matrizGauss./16;
% 3.1
figure
set(gcf, 'Name', '3.1 Filtro promedio y gaussiano', 'NumberTitle', 'Off');
img2 = imread('testpattern2.png');
%aplicacion de los filtros
imgPromedio = imfilter(img2, filtroProm);
imgGaussiano = imfilter(img2, filtroGauss);
subplot(2,2,1),imshow(img2, []),title('Imagen Original');
subplot(2,2,3),imshow(imgPromedio, []),title('Filtro Promedio');
subplot(2,2,4),imshow(imgGaussiano, []),title('Filtro Gaussiano');
%3.2
figure
set(gcf, 'Name', '3.2 Filtro promedio y gaussiano', 'NumberTitle', 'Off');
subplot(2,2,1),imshow(imgGaussiano, []),title('Filtro Gaussiano Original');
% Ventana 3x3
gauss3 = fspecial('gaussian', [3 3], 2);
imgGaussiano3 = imfilter(img2, gauss3, 'replicate');
subplot(2,2,2),imshow(imgGaussiano3, []),title('Filtro Gaussiano 3x3');
% Ventana 15x15
gauss15 = fspecial('gaussian', [15 15], 2);
imgGaussiano15 = imfilter(img2, gauss15, 'replicate');
subplot(2,2,3),imshow(imgGaussiano15, []),title('Filtro Gaussiano 15x15');
% Ventana 49x49
gauss49 = fspecial('gaussian', [49 49], 2);
imgGaussiano49 = imfilter(img2, gauss49, 'replicate');
subplot(2,2,4),imshow(imgGaussiano49, []),title('Filtro Gaussiano 49x49');
%3.3
figure
set(gcf, 'Name', '3.3 Filtro promedio y gaussiano', 'NumberTitle', 'Off');
subplot(2,2,1),imshow(imgGaussiano, []),title('Filtro Gaussiano Original');
% desviaci?n .2
desviacion2 = fspecial('gaussian', [5 5], 0.2);
imgDesviacion2 = imfilter(img2, desviacion2, 'replicate');
subplot(2,2,2),imshow(imgDesviacion2, []),title('Filtro Gaussiano desviac?n = 0.2');
% desviaci?n 1
desviacion1 = fspecial('gaussian', [5 5], 1);
imgDesviacion1 = imfilter(img2, desviacion1, 'replicate');
subplot(2,2,3),imshow(imgDesviacion1, []),title('Filtro Gaussiano desviac?n = 1');
% desviaci?n 10
desviacion10 = fspecial('gaussian', [5 5], 10);
imgDesviacion10 = imfilter(img2, desviacion10, 'replicate');
subplot(2,2,4),imshow(imgDesviacion10, []),title('Filtro Gaussiano desviac?n = 10');
%% Parte 4
% Some theory https://bohr.wlu.ca/hfan/cp467/12/notes/cp467_12_lecture6_sharpening.pdf
clear; clc;
% First test
[img, f_img] = highBoost('testpattern2.png', 1.5);
figure
subplot(2,2,1);  imshow(img); title('Original');
subplot(2,2,2);  imshow(f_img); title('High Boost (A = 1.5)');

% Second test
[img, f_img] = highBoost('testpattern2.png', 2);
subplot(2,2,3);  imshow(f_img); title('High Boost (A = 2)');

% Third test
[img, f_img] = highBoost('testpattern2.png', 5);
subplot(2,2,4);  imshow(f_img); title('High Boost (A = 5)');
set(gcf, 'Name', 'Filtro High Boost', 'NumberTitle', 'Off');


%% Parte 5
clear; clc;

img = imread('testpattern2.png');
figure

% Roberts
roberts_v = edge(img,'Roberts',[],'vertical');
roberts_h = edge(img,'Roberts',[],'horizontal');
roberts_2 = edge(img,'Roberts',[],'both');
subplot(3,3,7); imshow(roberts_v,[]); title('Roberts Vertical');
subplot(3,3,8); imshow(roberts_h,[]); title('Roberts Horizontal');
subplot(3,3,9); imshow(roberts_2,[]); title('Roberts Bidireccional');

% Prewitt
prewitt_v = edge(img,'Prewitt',[],'vertical');
prewitt_h = edge(img,'Prewitt',[],'horizontal');
prewitt_2 = edge(img,'Prewitt',[],'both');
subplot(3,3,4); imshow(prewitt_v,[]); title('Prewitt Vertical');
subplot(3,3,5); imshow(prewitt_h,[]); title('Prewitt Horizontal');
subplot(3,3,6); imshow(prewitt_2,[]); title('Prewitt Bidireccional');

% Sobel 
sobel_v = edge(img,'Sobel',[],'vertical');
sobel_h = edge(img,'Sobel',[],'horizontal');
sobel_2 = edge(img,'Sobel',[],'both');
subplot(3,3,1); imshow(sobel_v,[]); title('Sobel Vertical');
subplot(3,3,2); imshow(sobel_h,[]); title('Sobel Horizontal');
subplot(3,3,3); imshow(sobel_2,[]); title('Sobel Bidireccional');

set(gcf, 'Name', 'Filtros Pasa Altas', 'NumberTitle', 'Off');

%% Parte 6
close all; clear; clc;

total = 0;
temp_image = 0;
for folder = 1 : 5
    
    folder_cursor = sprintf('fotosruido%d', folder);
    cursor = strcat(folder_cursor, '/Picture %d.jpg');
    disp(strcat('Leyendo carpeta:', folder_cursor, '/...'));
    
    % Open each folder to get the images
    for imgn = 1 : 99
        image_cursor = sprintf(cursor, ((folder - 1) * 100 ) + imgn); % The folder changes in 100 number leap.
        im = imread(image_cursor);
        gray_image = double(rgb2gray(im));        
        total = gray_image + total;
        if(imgn == 1) % Help with histogram
            temp_image = gray_image;
        end
    end
    
    % Divide in the image analyzed
    total = total./imgn;
    
    % Calculate the histogram
    img_hist = total - temp_image;
    h = img_hist(:);
    figure
    % Image without noise
    subplot(1,2,1); imshow(total,[]); title(strcat('Imagenes ', num2str(((folder - 1) * 100 )), ':', num2str(((folder - 1) * 100) + 99)));
    subplot(1,2,2); histogram(h, 50); title('Histograma del ruido');
    
    set(gcf, 'Name', strcat(folder_cursor, '/*.jgp'), 'NumberTitle', 'Off');

    
    disp('Enter para la siguiente imagen');
    pause;
    
end

disp('Recordemos que el ruido en una imagen son pixeles aberrantes; no representan color, exposici?n o no aportan nada a la escena.');
disp('Juntar m?s im?genes en una provoca que los ruidos se vayan sumando o restando con la informaci?n importante que a la larga reduce el ruido.')

