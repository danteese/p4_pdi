function [img, f_image] = highBoost(image, A)
    f_hb = [
        -1.0    -1.0    -1.0; 
        -1.0    A + 8  -1.0; 
        -1.0    -1.0    -1.0]
    img = imread(image);
    f_image = imfilter(img, f_hb);
end

